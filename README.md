# exp-distance-reserve

*Notice*

Due to the ambiguous of the requirement about the "Manhattan distance",
I decided to make a runnable version of the requirement without the
distancing algorithm.

**Which is included?**

* Monorepo-ready, with:
  * Separating the `services`;
  * Using `internal` to prevent cross-service import;
* UT-ready, with:
  * Applying the Table-Driven Testing pattern;
  * Mocking with `mockery`
* `Makefile` is included to help common use case.

**Which isn't included?**

* No framework or boilerplate reference, I build the pure one myself.
* No authenticate & authorization.
* No panic recovery.
* No (much) input validation.
* Not designed for distributed & persistent operating.
