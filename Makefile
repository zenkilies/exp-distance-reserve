.DEFAULT_GOAL := default

default:
	@echo "Hello, world!"

protoc:
	@protoc --go_out=plugins=grpc:. --go_opt=paths=source_relative services/reserve/dto/reserve.proto

start_reserve:
	@cd services/reserve && go run main.go
