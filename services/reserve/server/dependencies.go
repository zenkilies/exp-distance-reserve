package server

import (
	"fmt"
	"log"

	"gitlab.com/zenkilies/exp-distance-reserve/services/reserve/internal/config"
	"gitlab.com/zenkilies/exp-distance-reserve/services/reserve/internal/handler"
	"gitlab.com/zenkilies/exp-distance-reserve/services/reserve/internal/resource/seat"
)

type depsCollector struct {
	// Resources
	seatResource seat.Resource

	// Handlers
	handler handler.Handler
}

func initDeps() depsCollector {
	errorHandler := func(name string, err error) {
		log.Fatal(fmt.Sprintf("failed to init %s with error: %s", name, err.Error()))
	}

	seatResource, err := seat.NewResource(
		uint64(config.GetInt(config.CINEMA_ROWS, 3)),
		uint64(config.GetInt(config.CINEMA_COLUMNS, 3)),
	)
	if err != nil {
		errorHandler("seat.Resource", err)
	}

	seatHandler, err := handler.NewHandler(
		handler.WithSeatResource(seatResource),
	)
	if err != nil {
		errorHandler("handler.Handler", err)
	}

	return depsCollector{
		seatResource: seatResource,
		handler:      seatHandler,
	}
}
