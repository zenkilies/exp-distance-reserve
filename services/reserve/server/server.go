package server

import (
	"context"

	"github.com/golang/protobuf/ptypes/empty"
	"google.golang.org/grpc"

	"gitlab.com/zenkilies/exp-distance-reserve/services/reserve/dto"
	"gitlab.com/zenkilies/exp-distance-reserve/services/reserve/internal/handler"
)

func NewServer() *grpc.Server {
	deps := initDeps()

	s := grpc.NewServer()
	dto.RegisterReserveServer(s, &server{
		reserveHandler: deps.handler,
	})

	return s
}

type server struct {
	dto.UnimplementedReserveServer
	reserveHandler handler.Handler
}

func (s *server) GetAvailableSeats(ctx context.Context, req *dto.GetAvailableSeatsRequest) (*dto.GetAvailableSeatsResponse, error) {
	return s.reserveHandler.GetAvailableSeats(ctx, req)
}

func (s *server) Ping(ctx context.Context, req *dto.PingRequest) (*dto.PingResponse, error) {
	return s.reserveHandler.Ping(ctx, req)
}

func (s *server) ReserveSeats(ctx context.Context, req *dto.ReserveSeatsRequest) (*empty.Empty, error) {
	return &empty.Empty{}, s.reserveHandler.ReserveSeats(ctx, req)
}
