package main

import (
	"fmt"
	"log"
	"net"

	"github.com/joho/godotenv"

	"gitlab.com/zenkilies/exp-distance-reserve/services/reserve/internal/config"
	"gitlab.com/zenkilies/exp-distance-reserve/services/reserve/server"
)

func main() {
	if err := godotenv.Load(); err != nil {
		fmt.Println(fmt.Sprintf("WARN: `godotenv.Load` has been failed: %s", err.Error()))
	}

	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", config.GetInt(config.PORT, 50051)))
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	s := server.NewServer()
	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
