package seat

type Seat struct {
	Row    uint64
	Column uint64
}
