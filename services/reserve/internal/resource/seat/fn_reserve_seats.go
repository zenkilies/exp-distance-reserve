package seat

import "fmt"

func (i *resourceImpl) ReserveSeats(seats []Seat) error {
	i.mutex.Lock()
	defer i.mutex.Unlock()

	for _, seat := range seats {
		if seat.Row < 1 || seat.Row > i.rows || seat.Column < 1 || seat.Column > i.columns {
			return fmt.Errorf("invalid seat at row: %d; column: %d", seat.Row, seat.Column)
		}

		if _, ok := i.reservedSeats[i.hashSeatIndex(seat.Row, seat.Column)]; ok {
			return fmt.Errorf("unavailable seat at row: %d; column: %d", seat.Row, seat.Column)
		}
	}

	for _, seat := range seats {
		i.reservedSeats[i.hashSeatIndex(seat.Row, seat.Column)] = true
	}

	return nil
}
