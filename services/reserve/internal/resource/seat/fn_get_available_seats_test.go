package seat

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetAvailableSeats(t *testing.T) {
	type testCase struct {
		desc     string
		rows     uint64
		columns  uint64
		doMocks  func(i *resourceImpl)
		expected []Seat
	}

	testCases := []testCase{
		{
			desc:    "no reserved seat",
			rows:    3,
			columns: 3,
			expected: []Seat{
				{Row: 1, Column: 1}, {Row: 1, Column: 2}, {Row: 1, Column: 3},
				{Row: 2, Column: 1}, {Row: 2, Column: 2}, {Row: 2, Column: 3},
				{Row: 3, Column: 1}, {Row: 3, Column: 2}, {Row: 3, Column: 3},
			},
		},
		{
			desc:    "has one reserved seat",
			rows:    3,
			columns: 3,
			doMocks: func(i *resourceImpl) {
				i.reservedSeats[i.hashSeatIndex(1, 1)] = true
			},
			expected: []Seat{
				{Row: 1, Column: 2}, {Row: 1, Column: 3},
				{Row: 2, Column: 1}, {Row: 2, Column: 2}, {Row: 2, Column: 3},
				{Row: 3, Column: 1}, {Row: 3, Column: 2}, {Row: 3, Column: 3},
			},
		},
		{
			desc:    "has two reserved seats",
			rows:    3,
			columns: 3,
			doMocks: func(i *resourceImpl) {
				i.reservedSeats[i.hashSeatIndex(1, 1)] = true
				i.reservedSeats[i.hashSeatIndex(2, 3)] = true
			},
			expected: []Seat{
				{Row: 1, Column: 2}, {Row: 1, Column: 3},
				{Row: 2, Column: 1}, {Row: 2, Column: 2},
				{Row: 3, Column: 1}, {Row: 3, Column: 2}, {Row: 3, Column: 3},
			},
		},
	}

	for _, testCase := range testCases {
		testCase := testCase
		t.Run(testCase.desc, func(t *testing.T) {
			res, _ := NewResource(testCase.rows, testCase.columns)
			if testCase.doMocks != nil {
				testCase.doMocks(res.(*resourceImpl))
			}

			seats := res.GetAvailableSeats()

			assert.Equal(t, testCase.expected, seats)
		})
	}
}
