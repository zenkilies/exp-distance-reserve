package seat

import "fmt"

func (i *resourceImpl) hashSeatIndex(row uint64, column uint64) string {
	return fmt.Sprintf("%d.%d", row, column)
}
