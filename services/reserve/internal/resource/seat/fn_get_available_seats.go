package seat

func (i *resourceImpl) GetAvailableSeats() []Seat {
	var res []Seat

	for x := uint64(1); x <= i.rows; x++ {
		for y := uint64(1); y <= i.columns; y++ {
			if _, ok := i.reservedSeats[i.hashSeatIndex(x, y)]; !ok {
				res = append(res, Seat{Row: x, Column: y})
			}
		}
	}

	return res
}
