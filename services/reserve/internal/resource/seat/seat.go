package seat

import (
	"fmt"
	"sync"
)

//go:generate mockery -name=Resource -inpkg -case=snake
type Resource interface {
	GetAvailableSeats() []Seat
	ReserveSeats([]Seat) error
}

func NewResource(rows uint64, columns uint64) (Resource, error) {
	res := &resourceImpl{
		mutex:         sync.Mutex{},
		rows:          rows,
		columns:       columns,
		reservedSeats: map[string]bool{},
	}

	if err := res.validateProperties(); err != nil {
		return nil, err
	}

	return res, nil
}

type resourceImpl struct {
	mutex sync.Mutex

	rows    uint64
	columns uint64

	reservedSeats map[string]bool
}

func (i *resourceImpl) validateProperties() error {
	if i.rows < 1 {
		return fmt.Errorf("invalid `rows` property (%d)", i.rows)
	}

	if i.columns < 1 {
		return fmt.Errorf("invalid `columns` property (%d)", i.columns)
	}

	return nil
}
