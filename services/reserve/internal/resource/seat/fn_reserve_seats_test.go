package seat

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestReserveSeats(t *testing.T) {
	type testCase struct {
		desc      string
		rows      uint64
		columns   uint64
		doMocks   func(i *resourceImpl)
		doAsserts func(i *resourceImpl)
		seats     []Seat
		expected  error
	}

	testCases := []testCase{
		{
			desc:    "happy path",
			rows:    3,
			columns: 3,
			doAsserts: func(i *resourceImpl) {
				assert.Equal(t, i.reservedSeats, map[string]bool{
					i.hashSeatIndex(1, 1): true,
				})
			},
			seats: []Seat{
				{Row: 1, Column: 1},
			},
			expected: nil,
		},
		{
			desc:    "happy path, with reserved seats",
			rows:    3,
			columns: 3,
			doMocks: func(i *resourceImpl) {
				i.reservedSeats[i.hashSeatIndex(2, 2)] = true
			},
			doAsserts: func(i *resourceImpl) {
				assert.Equal(t, i.reservedSeats, map[string]bool{
					i.hashSeatIndex(2, 2): true,
					i.hashSeatIndex(1, 1): true,
				})
			},
			seats: []Seat{
				{Row: 1, Column: 1},
			},
			expected: nil,
		},
		{
			desc:    "failed, invalid seats in row, below margin",
			rows:    3,
			columns: 3,
			seats: []Seat{
				{Row: 0, Column: 1},
			},
			expected: errors.New("invalid seat at row: 0; column: 1"),
		},
		{
			desc:    "failed, invalid seats in column, below margin",
			rows:    3,
			columns: 3,
			seats: []Seat{
				{Row: 1, Column: 0},
			},
			expected: errors.New("invalid seat at row: 1; column: 0"),
		},
		{
			desc:    "failed, invalid seats in row, above margin",
			rows:    3,
			columns: 3,
			seats: []Seat{
				{Row: 4, Column: 1},
			},
			expected: errors.New("invalid seat at row: 4; column: 1"),
		},
		{
			desc:    "failed, invalid seats in column, above margin",
			rows:    3,
			columns: 3,
			seats: []Seat{
				{Row: 1, Column: 4},
			},
			expected: errors.New("invalid seat at row: 1; column: 4"),
		},
		{
			desc:    "failed, reserve an unavailable seat",
			rows:    3,
			columns: 3,
			doMocks: func(i *resourceImpl) {
				i.reservedSeats[i.hashSeatIndex(2, 2)] = true
			},
			doAsserts: func(i *resourceImpl) {
				assert.Equal(t, i.reservedSeats, map[string]bool{
					i.hashSeatIndex(2, 2): true,
				})
			},
			seats: []Seat{
				{Row: 1, Column: 1},
				{Row: 2, Column: 2},
			},
			expected: errors.New("unavailable seat at row: 2; column: 2"),
		},
	}

	for _, testCase := range testCases {
		testCase := testCase
		t.Run(testCase.desc, func(t *testing.T) {
			res, _ := NewResource(testCase.rows, testCase.columns)
			if testCase.doMocks != nil {
				testCase.doMocks(res.(*resourceImpl))
			}

			err := res.ReserveSeats(testCase.seats)
			if testCase.doAsserts != nil {
				testCase.doAsserts(res.(*resourceImpl))
			}

			assert.Equal(t, testCase.expected, err)
		})
	}
}
