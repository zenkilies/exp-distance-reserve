package config

type Key string

const (
	PORT = Key("PORT")

	CINEMA_ROWS    = Key("CINEMA_ROWS")
	CINEMA_COLUMNS = Key("CINEMA_COLUMNS")
)
