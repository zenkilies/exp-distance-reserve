package config

import (
	"os"
	"strconv"
)

func GetInt(k Key, defaultVal int) int {
	if v := os.Getenv(string(k)); v != "" {
		if i, err := strconv.Atoi(v); err == nil {
			return i
		}

		return defaultVal
	}

	return defaultVal
}
