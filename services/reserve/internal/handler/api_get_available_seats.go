package handler

import (
	"context"

	"gitlab.com/zenkilies/exp-distance-reserve/services/reserve/dto"
	"gitlab.com/zenkilies/exp-distance-reserve/services/reserve/internal/resource/seat"
)

func (i *handlerImpl) GetAvailableSeats(ctx context.Context, req *dto.GetAvailableSeatsRequest) (*dto.GetAvailableSeatsResponse, error) {
	seats := i.seatResource.GetAvailableSeats()

	return &dto.GetAvailableSeatsResponse{
		Seats: i.convertSeatsFromResourceToDTO(seats),
	}, nil
}

func (i *handlerImpl) convertSeatsFromResourceToDTO(resourceSeats []seat.Seat) []*dto.Seat {
	seats := make([]*dto.Seat, len(resourceSeats))
	for i := 0; i < len(resourceSeats); i++ {
		seats[i] = &dto.Seat{
			X: resourceSeats[i].Column,
			Y: resourceSeats[i].Row,
		}
	}

	return seats
}
