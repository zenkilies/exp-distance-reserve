package handler

import (
	"testing"

	"github.com/stretchr/testify/mock"

	"gitlab.com/zenkilies/exp-distance-reserve/services/reserve/internal/resource/seat"
)

type mockTestFuncDeps struct {
	seatResource *seat.MockResource
}

type mockTestFunc func(m mockTestFuncDeps)

func mockDeps() mockTestFuncDeps {
	seatResourceMock := &seat.MockResource{}

	return mockTestFuncDeps{
		seatResource: seatResourceMock,
	}
}

func mockNew(m mockTestFuncDeps) Handler {
	return &handlerImpl{
		seatResource: m.seatResource,
	}
}

func assertExpectationForMocks(t *testing.T, m mockTestFuncDeps) {
	mock.AssertExpectationsForObjects(t,
		m.seatResource,
	)
}
