package handler

import (
	"context"

	"gitlab.com/zenkilies/exp-distance-reserve/services/reserve/dto"
	"gitlab.com/zenkilies/exp-distance-reserve/services/reserve/internal/resource/seat"
)

func (i *handlerImpl) ReserveSeats(ctx context.Context, req *dto.ReserveSeatsRequest) error {
	resourceSeats := i.convertSeatsFromDTOToResource(req.Seats)

	return i.seatResource.ReserveSeats(resourceSeats)
}

func (i *handlerImpl) convertSeatsFromDTOToResource(seats []*dto.Seat) []seat.Seat {
	resourceSeats := make([]seat.Seat, len(seats))

	for i := 0; i < len(seats); i++ {
		resourceSeats[i] = seat.Seat{
			Row:    seats[i].Y,
			Column: seats[i].X,
		}
	}

	return resourceSeats
}
