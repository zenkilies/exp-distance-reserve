package handler

import "gitlab.com/zenkilies/exp-distance-reserve/services/reserve/internal/resource/seat"

type Option func(i *handlerImpl)

func WithSeatResource(seatResource seat.Resource) Option {
	return func(i *handlerImpl) {
		i.seatResource = seatResource
	}
}
