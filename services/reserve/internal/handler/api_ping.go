package handler

import (
	"context"
	"log"

	"gitlab.com/zenkilies/exp-distance-reserve/services/reserve/dto"
)

func (i *handlerImpl) Ping(ctx context.Context, in *dto.PingRequest) (*dto.PingResponse, error) {
	log.Printf("Received: %v", in.GetName())
	return &dto.PingResponse{Message: "Hello " + in.GetName()}, nil
}
