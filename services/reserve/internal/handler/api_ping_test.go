package handler

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/zenkilies/exp-distance-reserve/services/reserve/dto"
)

func TestPing(t *testing.T) {
	t.Parallel()

	type testCase struct {
		desc string
		req  *dto.PingRequest
		res  *dto.PingResponse
		err  error
	}

	testCases := []testCase{
		{
			desc: "happy path",
			req: &dto.PingRequest{
				Name: "World",
			},
			res: &dto.PingResponse{
				Message: "Hello World",
			},
			err: nil,
		},
	}

	for _, testCase := range testCases {
		testCase := testCase
		t.Run(testCase.desc, func(t *testing.T) {
			m := mockDeps()

			h := mockNew(m)
			res, err := h.Ping(context.TODO(), testCase.req)

			assert.Equal(t, testCase.res, res)
			assert.Equal(t, testCase.err, err)
		})
	}
}
