package handler

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/zenkilies/exp-distance-reserve/services/reserve/dto"
	"gitlab.com/zenkilies/exp-distance-reserve/services/reserve/internal/resource/seat"
)

func TestGetAvailableSeats(t *testing.T) {
	t.Parallel()

	type testCase struct {
		desc    string
		doMocks mockTestFunc
		req     *dto.GetAvailableSeatsRequest
		res     *dto.GetAvailableSeatsResponse
		err     error
	}

	testCases := []testCase{
		{
			desc: "happy path",
			doMocks: func(m mockTestFuncDeps) {
				m.seatResource.On("GetAvailableSeats").Return([]seat.Seat{{Row: 2, Column: 1}})
			},
			req: &dto.GetAvailableSeatsRequest{
				NumberOfSeats: 1,
			},
			res: &dto.GetAvailableSeatsResponse{
				Seats: []*dto.Seat{
					{X: 1, Y: 2},
				},
			},
			err: nil,
		},
	}

	for _, testCase := range testCases {
		testCase := testCase
		t.Run(testCase.desc, func(t *testing.T) {
			m := mockDeps()
			if testCase.doMocks != nil {
				testCase.doMocks(m)
			}

			h := mockNew(m)
			res, err := h.GetAvailableSeats(context.TODO(), testCase.req)

			assert.Equal(t, testCase.res, res)
			assert.Equal(t, testCase.err, err)

			assertExpectationForMocks(t, m)
		})
	}
}
