package handler

import (
	"context"
	"errors"
	"fmt"
	"strings"

	"gitlab.com/zenkilies/exp-distance-reserve/services/reserve/dto"
	"gitlab.com/zenkilies/exp-distance-reserve/services/reserve/internal/resource/seat"
)

type Handler interface {
	GetAvailableSeats(context.Context, *dto.GetAvailableSeatsRequest) (*dto.GetAvailableSeatsResponse, error)
	Ping(context.Context, *dto.PingRequest) (*dto.PingResponse, error)
	ReserveSeats(context.Context, *dto.ReserveSeatsRequest) error
}

func NewHandler(opts ...Option) (Handler, error) {
	impl := &handlerImpl{}
	for _, opt := range opts {
		opt(impl)
	}

	err := validateDeps(impl)
	if err != nil {
		return nil, err
	}

	return impl, nil
}

type handlerImpl struct {
	seatResource seat.Resource
}

func validateDeps(i *handlerImpl) error {
	var missingDeps []string

	if i.seatResource == nil {
		missingDeps = append(missingDeps, "seat.Resource")
	}

	if len(missingDeps) > 0 {
		return errors.New(fmt.Sprintf("missing required dependencies: %s", strings.Join(missingDeps, ", ")))
	}

	return nil
}
