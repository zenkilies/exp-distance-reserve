package handler

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/zenkilies/exp-distance-reserve/services/reserve/dto"
	"gitlab.com/zenkilies/exp-distance-reserve/services/reserve/internal/resource/seat"
)

func TestReserveSeats(t *testing.T) {
	t.Parallel()

	type testCase struct {
		desc    string
		doMocks mockTestFunc
		req     *dto.ReserveSeatsRequest
		err     error
	}

	testCases := []testCase{
		{
			desc: "happy path",
			doMocks: func(m mockTestFuncDeps) {
				m.seatResource.On("ReserveSeats", []seat.Seat{
					{Row: 1, Column: 1},
				}).Return(nil)
			},
			req: &dto.ReserveSeatsRequest{
				Seats: []*dto.Seat{
					{X: 1, Y: 1},
				},
			},
			err: nil,
		},
	}

	for _, testCase := range testCases {
		testCase := testCase
		t.Run(testCase.desc, func(t *testing.T) {
			m := mockDeps()
			if testCase.doMocks != nil {
				testCase.doMocks(m)
			}

			h := mockNew(m)
			err := h.ReserveSeats(context.TODO(), testCase.req)

			assert.Equal(t, testCase.err, err)
		})
	}
}
