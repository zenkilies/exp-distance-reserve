module gitlab.com/zenkilies/exp-distance-reserve

go 1.12

require (
	github.com/golang/protobuf v1.4.2
	github.com/joho/godotenv v1.3.0
	github.com/stretchr/testify v1.4.0
	github.com/vektra/mockery v1.1.2 // indirect
	golang.org/x/lint v0.0.0-20190313153728-d0100b6bd8b3 // indirect
	google.golang.org/grpc v1.26.0
	honnef.co/go/tools v0.0.0-20190523083050-ea95bdfd59fc // indirect
)
